# DepsManager

Ruby gem that would collect all local gems in your project and send them to a web server to determine the system libraries you need to install.

## Installation

To install a gem from gitlab, first you need to install `specific_install`

```ruby
$ gem install specific_install
```

Then you can install the gem (prefered in your global gemset) by:

```ruby
$ gem specific_install -l https://gitlab.com/alazhary/gem_deps.git
```

## Usage
First: navigate to the project path containing the Gemfile
```
$ cd TO/PROJECT/PATH
```
Then run the following command:
```
$ irb -Ilib -rdeps_manager
2.4.0 :001 > DepsManager.launch
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://github.com/alazhary/gem_deps This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the DepsManager project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/alazhary/gem_deps/blob/master/CODE_OF_CONDUCT.md).
