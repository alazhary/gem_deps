module DepsManager
    class GemsCollector
        def self.collect_os_info
            @os ||= (
                host_os = RbConfig::CONFIG['host_os']
                case host_os
                    when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
                        :windows
                    when /darwin|mac os/
                        :mac
                    when /linux/
                        :linux
                    when /solaris|bsd/
                        :unix
                    else
                        raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
                end
                )
        end

        def self.collect_gems(gemPath='Gemfile')
            @gemList=[]
            File.open(gemPath).each do |line|
                (line[0..3]=='gem ') ? @gemList << line[5..line[5..-1].index("'")+4]:0
            end
            return @gemList
        end
    end
end