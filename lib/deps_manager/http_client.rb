require 'faraday'
require 'json'

module DepsManager
  class HttpClient
    @@conn=Faraday.new(url:'http://localhost:1337')
    def self.get_deps_from_server(gems)
        @res = @@conn.post '/gemdeps/getMyDeps', {gemList:gems}
        return JSON.parse(@res.body)
    end

    def self.present_deps(resp,os)
        resp.each do |gem|
            gem["gem_deps"].each do |obj| next unless obj.has_key?os
              puts "your deps for '#{gem["gem_name"]}' : #{obj[os]}" 
            end
        end
    end

  end
end
