require "deps_manager/gems_collector"
require "deps_manager/http_client"
require 'rbconfig'

module DepsManager
  module_function
  def launch
    @os=GemsCollector.collect_os_info
    @gems=GemsCollector.collect_gems
    @resp=HttpClient.get_deps_from_server(@gems)
    HttpClient.present_deps(@resp,@os.to_s)
  end

end