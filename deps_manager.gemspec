# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "deps_manager/version"

Gem::Specification.new do |spec|
  spec.name          = "deps_manager"
  spec.version       = "0.1.0"
  spec.authors       = ["Mohammed"]
  spec.email         = ["mohammed.m.alazhary@gmail.com"]

  spec.summary       = %q{Ruby gem that would collect all
                          local gems in your project and send them to a web server
                          to determine the system libraries you need to install.}
  spec.description   = %q{Collects the project dependencies and operating system information,
                          sends them to the web service (specifications below),
                          and displays the required system libraries to install
                          all your dependencies}
  spec.homepage      = "https://gitlab.com/alazhary/gem_deps.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://gemsdeps.herokuapp.com/"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = ["lib/deps_manager.rb","lib/deps_manager/gems_collector.rb","lib/deps_manager/http_client.rb"]
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_dependency "faraday", "~> 0.9.2"
end
